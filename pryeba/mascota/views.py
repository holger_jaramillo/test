from . models import Mascota, Tipo
from django.shortcuts import render
from propietario.models import *
# Create your views here.



def inicio(request, *args, **kwargs):

    if request.method == "POST":
        nombre = request.POST.get('nombre')
        edad = request.POST.get('edad')
        propietarios = request.POST.getlist('propietarios')
        tipo = request.POST.get('tipo')
        tipoDb = Tipo.objects.get(id=tipo)
        mascota = Mascota(nombre=nombre,edad=edad,tipo=tipoDb)
        mascota.save()

        for p in propietarios:
            propietario = User.objects.get(id=p)
            mascota.propietario.add(propietario)

        mascota.save()
    propietarios = User.objects.all()
    mascotas = Mascota.objects.all().order_by('-id')
    tipos = Tipo.objects.all()

    return render(request,'mascota/mascota.html',{
        "propietarios":propietarios,
        "mascotas": mascotas,
        "tipos":tipos
    })




def tipos(request, *args, **kwargs):

    if request.method == "POST":
        tipoReq = request.POST.get('tipo')
        
        tipo = Tipo(tipo=tipoReq)
        tipo.save()

    tipos = Tipo.objects.all().order_by('-id')

    return render(request,'mascota/tipos.html',{
        "tipos": tipos
    })