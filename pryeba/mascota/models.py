from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Tipo(models.Model):
    tipo = models.CharField(max_length=50)

    
class Mascota(models.Model):
    nombre = models.CharField( max_length=100)
    edad = models.IntegerField(default=0)
    propietario = models.ManyToManyField(User)

    tipo = models.ForeignKey(Tipo,on_delete=models.CASCADE)


