
from django.shortcuts import render
from django.contrib.auth.models import User

from propietario.models import DatosAdicionales
from mascota.models import Tipo, Mascota

# Create your views here.

def reportes(request, *args, **kwargs):
    tipos = Tipo.objects.all()
    if request.method == "POST":
        reporte = request.POST.get('reporte')

        if reporte == "tipo":
            tipo = request.POST.get('tipo')
            tipoR = Tipo.objects.get(id=tipo)
            
            mascotas = Mascota.objects.filter(tipo=tipoR)

            numP = []
            for m in mascotas:
                #numUser = User.objects.filter(id=m.propietario)
                for p in m.propietario.all():
                    numP.append(p.id)
            
            numeroPersonas= len(sorted(set(numP)))


            return render(request,'reporte/reporte.html',{
                "tipos":tipos,
                "numeroPersonas":numeroPersonas,
                "tipoMascota":tipoR
            })

        if reporte == "mascotas":
            edad = request.POST.get('edad')
            mascotasRep = Mascota.objects.filter(edad__gt=int(edad)) 
            return render(request,'reporte/reporte.html',{
                "tipos":tipos,
                "mascotas":mascotasRep
            })
        
    return render(request,'reporte/reporte.html',{
        "tipos":tipos
    })