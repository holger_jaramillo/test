from django.shortcuts import render
from django.contrib.auth.models import User
from . models import DatosAdicionales
# Create your views here.

def inicio(request, *args, **kwargs):

    if request.method == "POST":
        nombre = request.POST.get('nombre')
        apellido = request.POST.get('apellido')
        cedula = request.POST.get('cedula')
        direccion = request.POST.get('direccion')
        telefono = request.POST.get('telefono')

        propietario = User()
        propietario.username = nombre.lower()
        propietario.first_name = nombre
        propietario.last_name = apellido

        propietario.save()

        datosAdicionales = DatosAdicionales(user= propietario,cedula = cedula,direccion = direccion,telefono = telefono)

        datosAdicionales.save()


    propietarios = DatosAdicionales.objects.all()
        



    return render(request,'propietario/propietario.html',{
        "propietarios":propietarios
    })