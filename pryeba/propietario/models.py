from django.db import models
from django.db.models.base import Model
from django.contrib.auth.models import User

# Create your models here.

class DatosAdicionales(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    cedula = models.CharField(max_length=15)
    direccion = models.CharField(max_length=255)
    telefono = models.CharField(max_length=20)